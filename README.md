# SSP_mala_prace
"Malá" práce z předmětu SSP.
Projket ukazující 
- [ ] Funkci CI
- [ ] Generovaní HTML stránky z Markdown podkladu
- [ ] Test kvality Markdown souborů
####
[Pages](https://test7071.gitlab.io/ssp_mala_prace) 
## Použití
V projektu je složka [holy_pages](https://gitlab.com/test7071/ssp_mala_prace/-/tree/main/holy_pages) ve které se nachází markdown struktura, ze které je potom vytvořen pomocí CI pipeliny statický .html content.
Po úpravě dojde k automatické kotrole a vytvoření. 
## Instalace
Není potřeba protože projekt jede na Gitlab za pomocí jejich pipeline.
Pokud kchcete spusti projekt lok8ln2 tak jen za použití vlastních pipeline.
## Prerekvizity
- [ ] Python enviroment pro mkdocs
- [ ] GitLab repo
## Závislosti
Jsou v [required_trojan.txt](https://gitlab.com/test7071/ssp_mala_prace/-/blob/main/required_trojan.txt) 
```
# Documentation static site generator & deployment tool
mkdocs>=1.1.2
```
